#include <Arduino.h>

/**
 * Array of relevant input pins.
 * The first four pins are the four switches,
 * followed by the activate pin.
 */
static int in_pins[] = {2, 3, 4, 5, 11};
#define N_SWITCHES 4
#define ACTIVATE 4

static int out_pin = 12;

#define N_PINS (sizeof(in_pins) / sizeof(*in_pins))

static int in_pin_states[N_PINS];

#ifndef DEBOUNCING
#define DEBOUNCING 15
#endif

// Loop interval in milliseconds
#ifndef INTERVAL
#define INTERVAL 1
#endif

static bool activated = false;

// Countdown for debouncing
static int in_pin_countdown[N_PINS];

#define INPUT_ACTIVE LOW
#define OUTPUT_ACTIVE LOW

static int progress = 0;

static void on_input_change(int index, int level) {
  if (index < N_SWITCHES) {
    Serial.print("switch[");
    Serial.print(index);
    Serial.print("]: ");
    Serial.println(level);
    if (activated && level == INPUT_ACTIVE && progress < N_SWITCHES) {
      if (index != progress) {
        progress = 0;
        Serial.println("Progress resetted");
      }
      if (index == progress) {
        progress++;
        Serial.print("progress: ");
        Serial.println(progress);
        if (progress == N_SWITCHES) {
          digitalWrite(out_pin, OUTPUT_ACTIVE);
          Serial.println("Riddle solved!");
          Serial.print("output: ");
          Serial.println(OUTPUT_ACTIVE);
        }
      }
    }
    Serial.println();
  }
  else if (index == ACTIVATE) {
    activated = level == INPUT_ACTIVE;
    Serial.print("activated: ");
    Serial.println(activated);
    if (!activated) {
      progress = 0;
      digitalWrite(out_pin, !OUTPUT_ACTIVE);
      Serial.println("Reset");
      Serial.print("output: ");
      Serial.println(!OUTPUT_ACTIVE);
      Serial.print("progress: ");
      Serial.println(progress);
    }
    Serial.println();
  }
}

void setup() {
  for (int i = 0; i < N_PINS; i++) {
    pinMode(in_pins[i], INPUT_PULLUP);
  }
  pinMode(out_pin, OUTPUT);
  digitalWrite(out_pin, !OUTPUT_ACTIVE);
  for (int i = 0; i < N_PINS; i++) {
    in_pin_states[i] = digitalRead(in_pins[i]);
    in_pin_countdown[i] = DEBOUNCING;
  }
  activated = in_pin_states[ACTIVATE] == INPUT_ACTIVE;
  Serial.begin(9600);
  while (!Serial);

  Serial.println("Initial state:");
  Serial.print("activated: ");
  Serial.println(activated);
  Serial.print("switch_pins: [");
  Serial.print(in_pin_states[0]);
  for (int i = 1; i < N_SWITCHES; i++) {
    Serial.print(", ");
    Serial.print(in_pin_states[i]);
  }
  Serial.println("]");
  Serial.print("progress: ");
  Serial.println(progress);
  Serial.print("output: ");
  Serial.println(!OUTPUT_ACTIVE);
  Serial.println();
}

void loop() {
  for (int i = 0; i < N_PINS; i++) {
    int new_state = digitalRead(in_pins[i]);
    if (new_state != in_pin_states[i]) {
      --in_pin_countdown[i];
      if (!in_pin_countdown[i]) {
        in_pin_states[i] = new_state;
        in_pin_countdown[i] = DEBOUNCING;
        on_input_change(i, new_state);
      }
    }
    else {
      in_pin_countdown[i] = DEBOUNCING;      
    }
  }
  delay(INTERVAL);
}
